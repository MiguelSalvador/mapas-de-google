package com.example.emiliocarranza

import android.graphics.Bitmap
import android.graphics.Canvas
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.content.ContextCompat
import com.example.emiliocarranza.databinding.ActivityMapsBinding

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.floatingactionbutton.FloatingActionButton

class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap

    private val templos = mutableListOf<Templo>()
    private lateinit var myLocationButton : FloatingActionButton
    private lateinit var binding: ActivityMapsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMapsBinding.inflate(layoutInflater)
        setContentView(binding.root)
        addTemplo()
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    private fun addTemplo() {
        templos.add(Templo("Templo ghetsemanì",19.968421904202085,-96.61317773100834))
        templos.add(Templo("Templo los hijos de dios",19.970180514409762,-96.61506650257714))
        templos.add(Templo("Templo el sol", 19.970753268958052,-96.61291158605465))
        templos.add(Templo("Templo de Dios",19.97056772920115,-96.61061071926387))
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        val icon = getIglesiaIcon()

        for (templo in templos){
            val temploPosition = LatLng(templo.latitud,templo.longitud)
            val temploName = templo.name

            val markerOptions = MarkerOptions().position(temploPosition).title(temploName)
                    .icon(icon)
            mMap.addMarker(markerOptions)
        }

        // Add a marker in Sydney and move the camera
        val carranza = LatLng(19.967012177814098, -96.61375489872948)
        mMap.addMarker(MarkerOptions().position(carranza).title("Marker in Emilio Carranza"))
        mMap.moveCamera(CameraUpdateFactory.newLatLng(carranza))

        binding.myLocation.setOnClickListener {
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(carranza,13.0f))

        }
    }

    private fun getIglesiaIcon(): BitmapDescriptor {
        val drawable = ContextCompat.getDrawable(this, R.drawable.ic_iglesia)
        drawable?.setBounds(0,0,drawable.intrinsicWidth,drawable.intrinsicHeight)
        val bitmap = Bitmap.createBitmap(drawable?.intrinsicWidth ?: 0,
                drawable?.intrinsicHeight ?: 0, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        drawable?.draw(canvas)
        return BitmapDescriptorFactory.fromBitmap(bitmap)
    }
}